# ABOUT
This project is a library that facilitates the soft interrupt of long-running
CUDA kernels. To achieve this a time limit is enforced on the kernel's
execution. This this limit has expired the kernel returns control to the host
where a device-to-host copy of the kernel's data can be made. This method works
because it takes advantage of CUDA's partitioning of threads into blocks. After
blocks are launched a check is done to determine whether the execution time has
expired or not. If expired, the block immediately exits without performing any
work. 

### How it works - macros
Macros are used to rewrite kernel launches and kernel definitions in a way that
facilitates suspension and resumption.  There are currently 3 MACROS
responsible for this:

1. BACKUP\_KERNEL\_LAUNCH
2. BACKUP\_KERNEL\_DEF
3. BACKUP\_CONTINUE

#### BACKUP\_KERNEL\_LAUNCH
This macro performs two primary purposes:

1. Augments the kernel's argument list.
2. Wraps kernel in execution loop.

The arguments introduced are a timeout variable and a boolean array that keeps
track of executed blocks. The execution loop iteratively executes the kernel
until all blocks have completed. After making an asynchronous kernel launch a
monitoring function is called. This function sleeps until the quantum has
expired and then sets a timeout flag to signal the kernel to not allow any
newly launched blocks to continue.  After the kernel returns, the boolean array
is checked to determine if all blocks completed. If all blocks completed no
further launches are made, otherwise the execution loop continues.

_**Example**_

*An existing kernel invocation:*

```
vectorAdd<<<blocksPerGrid, threadsPerBlock>>>(d_A, d_B, d_C, numElements);
```

*Is surrounded by:*

```
BACKUP_KERNEL_LAUNCH(vectorAdd, blocksPerGrid, threadsPerBlock, 0,0, d_A, d_B, d_C, numElements, d_threads, thread_count);
```

*And transformed into:*

```
BACKUP_init( & BACKUP_timeout, & BACKUP_block_info, 0.0001, & BACKUP_complete, blocksPerGrid);
while (!BACKUP_complete) 
{
  vectorAdd << < blocksPerGrid, threadsPerBlock, 0, 0 >>> (BACKUP_timeout, BACKUP_block_info, d_A, d_B, d_C, numElements);
  BACKUP_monitor( & BACKUP_complete, "vectorAdd");
}
```
Where all variables prefixed by ``BACKUP`` were initialized in ``BACKUP_init()``.

#### BACKUP\_KERNEL\_DEF
This macro simply rewrites the default kernel's definition so that includes the
additional parameters added by the BACKUP\_KERNEL\_LAUNCH macro.

_**Example**_

*An existing kernel definition:*

```
__global__ void vectorAdd(const float *A, const float *B, float *C, long numElements)
```

*Is surrounded by:*

```
BACKUP_KERNEL_DEF(vectorAdd,const float *A, const float *B, float *C, long numElements, struct thread_time_t *threads, unsigned int *thread_count)
```

*And transformed into:*

```
__global__ void vectorAdd(volatile unsigned int *timeout, block_info *b_info,const float *A, const float *B, float *C, long numElements)

```

#### BACKUP\_CONTINUE
This macro needs to be placed in the first line of every kernel to determine
whether or not the kernel's execution should continue.

_**Example**_
*The macro placed called at the start of the kernel as follows:*

```
BACKUP_CONTINUE();
```
*Which is substituted by:*

```
__shared__ unsigned int block_time_out;
unsigned long long int bid = blockIdx.x + gridDim.x * (blockIdx.y + gridDim.z * blockIdx.z);
if (threadIdx.x == 0 && threadIdx.y == 0 && threadIdx.z == 0)
{
   block_time_out = *timeout;
}

if(is_block_executed[bid] == 1)
{
  return;
}
__syncthreads();

if(block_time_out == 1 && is_block_executed[bid] == 0)
{
  return;
}
is_block_executed[bid] = 1;

```

Where ``BACKUP_continue()`` is defined as part of the mechanism.

#### Macros for thread-level interruption
The macros described thus far are general-purpose and will work for all kernels
as interrupts are done at the level of kernel blocks. However, block-level
interruption causes a greater delay in the kernel's response to an interrupt
request. This is because after a block starts, the entire block, including
unscheduled warps, must run to completion. It is important for blocks to
complete in entirety to cover the case where synchronisation within the block
is desired (i.e., via the use of ``__syncthreads()``). On the other hand, if
the kernel requires no such synchronisations then interruptions can safely be
performed at the thread-level. Doing so reduces the interrupt response time as
only the currently executing threads need to complete instead of blocks. To
perform interrupts at the thread-level, the ``BACKUP_LAUNCH()`` and
``BACKUP_CONTINUE`` macro must be substituted with the following macros
respectively.

1. ``BACKUP_KERNEL_LAUNCH_THREAD`` and
2. ``BACKUP_CONTINUE_THREAD``

It is **important** for these macros to be used together. Using a macro that
launches the kernel for block interruptions with a macro for thread
interruptions is undefined and will most likely result in segmentation faults.

#### Kernels requiring template arguments
Kernels that require template arguments can use the same launch macro but will
need to surround the kernel name and template arguments with parentheses.  If a
kernel needs to be launched is defined as follows:

```
template <int N, int M>
__global__ void vectorAdd(const float *A, const float *B, float *C, long numElements)

```
*It is surrounded by*

```
template <int N, int M>
__global__ void
BACKUP_KERNEL_DEF(vectorAdd,const float *A, const float *B, float *C, long numElements, struct thread_time_t *threads, unsigned int *thread_count)
```
*And the launch needs to be modified from:*

```
vectorAdd<N,M><<<blocksPerGrid, threadsPerBlock>>>(d_A, d_B, d_C, numElements);
```
*to:*

```
BACKUP_KERNEL_LAUNCH((vectorAdd<N,M>) , blocksPerGrid, threadsPerBlock, 0, 0, d_A, d_B, d_C, numElements);
```

## Memory Copies on Interrupt
To perform memory copies on interrupt a list of allocations on the device is
kept. To build this list a wrapper around `cudaMalloc()` called
`BACKUP_cudaMalloc()` is used. At each interrupt the allocation list is
traversed and a device to host memory copy is performed for each entry.
Likewise, a wrapper for `cudaFree()` called `BACKUP_cudaFree()` is provided to
remove allocations from the list.

## GPU Interrupt Control
A callback function can be registered to be called each time the GPU is
interrupted. This is to provide the user with some control between interrupts.
Within the registered callback function there are API functions that can be
called to modified the behavior of interruptions. See the table below for a
brief description of the control functions.

Function                          |                       Description                          
----------------------------------|-------------------------------------------------------------
`BACKUP_stop_kernel_relaunch`     |  Stops kernel from being relaunched after current interrupt 
`BACKUP_stop_interrupting_kernel` |  Future interrupts of kernel will be disabled 
`BACKUP_config`                   |  Initial configurations can be redefined

Generate Doxygen documentation for more information on these functions. 

# Building and Installing
1. git clone https://bitbucket.org/maxbaird/cuda_backup/
2. mkdir cuda\_backup/build && cd cuda\_backup/build
3. cmake -DCMAKE\_INSTALL\_PREFIX:PATH=/install/path/cuda\_backup .. && make all install

## Run Example
The "build/examples" directory contains a modified and unmodified vector addition example.
Instructions can be found in the file "examples/README".

## Technical Documentation
Technical documentation of the code can be generated with Doxygen, to do this configure
with `-DENABLE_DOCU=ON` and execute `make doc` in the build directory.

## All CMAKE Options
Below are all CMAKE options and their defaults.

Option             |                       Description                           | Default
-------------------|-------------------------------------------------------------|---------
`ENABLE_EXAMPLES`  |  Enables the generation of examples                         |  ON                        
`ENABLE_DOCU`      |  Enables the generation of a Doxygen documentation          |  OFF
`ENABLE_DEBUG`     |  Enables debug mode                                         |  OFF
`ENABLE_HASH`      |  Enables access SHA-512 print MACRO to print a result hash  |  OFF

# Research Work
This is a research project, the details of which are available in the following
research paper: M. Baird, C. Fensch, S. Scholz, and A. Šinkarovs, __*"A
Lightweight Approach to GPU Resilience"*__ in Euro-Par 2018: Parallel
Processing Workshops, pp. 826–838, Springer International Publishing, 2019.

Last Modified Tue  3 Mar 21:53:58 GMT 2020
