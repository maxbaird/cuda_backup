/**
 * Vector addition: C = A + B.
 *
 * This sample is a very basic sample that implements element by element
 * vector addition. It is the same as the sample illustrating Chapter 2
 * of the programming guide with some additions like error checking.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cuda_backup.h>

#define CUDA_ERROR_CHECK(fun)                                                                   \
do{                                                                                             \
    cudaError_t err = fun;                                                                      \
    if(err != cudaSuccess)                                                                      \
    {                                                                                           \
      fprintf(stderr, "Cuda error %d %s:: %s\n", __LINE__, __func__, cudaGetErrorString(err));  \
      exit(EXIT_FAILURE);                                                                       \
    }                                                                                           \
}while(0);

/* Here the kernel takes the number of elements as a template argument */

template <int N>
__global__ void
BACKUP_KERNEL_DEF(vectorAdd, const float *A, const float *B, float *C, long numElements)
{
    BACKUP_CONTINUE();
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    if (i < numElements)
    {
        C[i] = A[i] + B[i] + N;
    }
}

void print_sum_c(float *c, long n)
{
  float sum = 0.0;

  for(long i = 0; i < n; i++){
    sum = sum + c[i];
  }

  fprintf(stdout, "Sum = %f\n", sum);
}

int main(int argc, char *argv[])
{
    if(argc != 2)
    {
      fprintf(stderr, "usage: %s <vector-length>\n", argv[0]);
      exit(EXIT_FAILURE);
    }

    // Print the vector length to be used, and compute its size
    long numElements = strtol(argv[1], NULL, 10);
    size_t size = numElements * sizeof(float);
    printf("[Vector addition of %ld elements]\n", numElements);

    float *h_A = (float *)malloc(size);
    float *h_B = (float *)malloc(size);
    float *h_C = (float *)malloc(size);

    if (h_A == NULL || h_B == NULL || h_C == NULL)
    {
        fprintf(stderr, "Failed to allocate host vectors!\n");
        exit(EXIT_FAILURE);
    }

    // Initialize the host input vectors
    for (int i = 0; i < numElements; ++i)
    {
        h_A[i] = rand()/(float)RAND_MAX;
        h_B[i] = rand()/(float)RAND_MAX;
        h_C[i] = 0.0;
    }

    float *d_A = NULL;
    float *d_B = NULL;
    float *d_C = NULL;

    /* Template argument for kernel */
    const int N = 5;

    CUDA_ERROR_CHECK(BACKUP_cudaMalloc((void **)&d_A, size));
    CUDA_ERROR_CHECK(BACKUP_cudaMalloc((void **)&d_B, size));
    CUDA_ERROR_CHECK(BACKUP_cudaMalloc((void **)&d_C, size));

    // Copy the host input vectors A and B in host memory to the device input vectors in
    // device memory
    CUDA_ERROR_CHECK(cudaMemcpy(d_A, h_A, size, cudaMemcpyHostToDevice));
    CUDA_ERROR_CHECK(cudaMemcpy(d_B, h_B, size, cudaMemcpyHostToDevice));
    CUDA_ERROR_CHECK(cudaMemcpy(d_C, h_C, size, cudaMemcpyHostToDevice));

    int threadsPerBlock = 256;
    int blocksPerGrid =(numElements + threadsPerBlock - 1) / threadsPerBlock;

    BACKUP_config(0.011, backupDoMemcpyTrue, backupIncreaseQuantumTrue, 0.001);
    BACKUP_KERNEL_LAUNCH((vectorAdd<N>), blocksPerGrid, threadsPerBlock, 0, 0, d_A, d_B, d_C, numElements);

    // Copy the device result vector in device memory to the host result vector
    // in host memory.
    CUDA_ERROR_CHECK(cudaMemcpy(h_C, d_C, size, cudaMemcpyDeviceToHost));

    // Verify that the result vector is correct
    for (int i = 0; i < numElements; ++i)
    {
        if (fabs(h_A[i] + h_B[i] + N - h_C[i]) > 1e-5)
        {
            fprintf(stderr, "Result verification failed at element %d!\n", i);
            exit(EXIT_FAILURE);
        }
    }

    printf("Test PASSED\n");

    // Free device global memory
    CUDA_ERROR_CHECK(cudaFree(d_A));
    CUDA_ERROR_CHECK(cudaFree(d_B));
    CUDA_ERROR_CHECK(cudaFree(d_C));

    // Free host memory
    free(h_A);
    free(h_B);
    free(h_C);

    CUDA_ERROR_CHECK(cudaDeviceReset());

    return EXIT_SUCCESS;
}

