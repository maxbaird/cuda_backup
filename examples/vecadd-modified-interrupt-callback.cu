/**
 * Vector addition: C = A + B.
 *
 * This sample is a very basic sample that implements element by element
 * vector addition. It is the same as the sample illustrating Chapter 2
 * of the programming guide with some additions like error checking.
 *
 * This also demonstrates how a callback function can be registered
 * to be called at each interrupt.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <cuda_backup.h>

#define CUDA_ERROR_CHECK(fun)                                                                   \
do{                                                                                             \
    cudaError_t err = fun;                                                                      \
    if(err != cudaSuccess)                                                                      \
    {                                                                                           \
      fprintf(stderr, "Cuda error %d %s:: %s\n", __LINE__, __func__, cudaGetErrorString(err));  \
      fflush(stderr);                                                                           \
      exit(EXIT_FAILURE);                                                                       \
    }                                                                                           \
}while(0);

typedef struct{
  int a;
  int b;
  int sum;
}args_t;

//Example of how a callback function may be registered to be called
//each time the kernel is interrupted.
void callback(void *args)
{
  args_t *a = (args_t*)args;
  a->sum = a->a + a->b;
}

/**
 * CUDA Kernel Device code
 *
 * Computes the vector addition of A and B into C. The 3 vectors have the same
 * number of elements numElements.
 */
__global__ void
BACKUP_KERNEL_DEF(vectorAdd, const float *A, const float *B, float *C, long numElements)
{
    BACKUP_CONTINUE();
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    if (i < numElements)
    {
        C[i] = A[i] + B[i];
    }
}

int main(int argc, char *argv[])
{
    if(argc != 2)
    {
      fprintf(stderr, "usage: %s <vector-length>\n", argv[0]);
      exit(EXIT_FAILURE);
    }

    // Print the vector length to be used, and compute its size
    long numElements = strtol(argv[1], NULL, 10);
    size_t size = numElements * sizeof(float);
    printf("[Vector addition of %ld elements]\n", numElements);

    float *h_A = (float *)malloc(size);
    float *h_B = (float *)malloc(size);
    float *h_C = (float *)malloc(size);

    if (h_A == NULL || h_B == NULL || h_C == NULL)
    {
        fprintf(stderr, "Failed to allocate host vectors!\n");
        exit(EXIT_FAILURE);
    }

    // Initialize the host input vectors
    for (int i = 0; i < numElements; ++i)
    {
        h_A[i] = rand()/(float)RAND_MAX;
        h_B[i] = rand()/(float)RAND_MAX;
        h_C[i] = 0.0;
    }

    float *d_A = NULL;
    float *d_B = NULL;
    float *d_C = NULL;

    CUDA_ERROR_CHECK(BACKUP_cudaMalloc((void **)&d_A, size));
    CUDA_ERROR_CHECK(BACKUP_cudaMalloc((void **)&d_B, size));
    CUDA_ERROR_CHECK(BACKUP_cudaMalloc((void **)&d_C, size));

    // Copy the host input vectors A and B in host memory to the device input vectors in
    // device memory
    CUDA_ERROR_CHECK(cudaMemcpy(d_A, h_A, size, cudaMemcpyHostToDevice));
    CUDA_ERROR_CHECK(cudaMemcpy(d_B, h_B, size, cudaMemcpyHostToDevice));
    CUDA_ERROR_CHECK(cudaMemcpy(d_C, h_C, size, cudaMemcpyHostToDevice));

    int threadsPerBlock = 256;
    int blocksPerGrid =(numElements + threadsPerBlock - 1) / threadsPerBlock;

    BACKUP_write_to_fs(true); /* Write GPU data to file system after every interrupt */
    double quantum = 0.011; /* How often should the kernel be interrupted */
    double quantum_increment = 0.001; /* By how much to increase quantum after interrupt */

    BACKUP_config(quantum, backupDoMemcpyTrue, backupIncreaseQuantumTrue, quantum_increment);

    args_t args = {2,3,0}; //Arguments for callback function
    BACKUP_register_callback(callback, (void *)&args); //How to register callback function with arguments.

    BACKUP_KERNEL_LAUNCH(vectorAdd, blocksPerGrid, threadsPerBlock, 0, 0, d_A, d_B, d_C, numElements);
    fprintf(stdout, "%d + %d = %d\n", args.a, args.b, args.sum);
    CUDA_ERROR_CHECK(cudaMemcpy(h_C, d_C, size, cudaMemcpyDeviceToHost));
    //BACKUP_PRINT_HASH(h_C, size);

    // Verify that the vector is correct
    bool correct = true;
    for (int i = 0; i < numElements; ++i)
    {
        if (fabs(h_A[i] + h_B[i] - h_C[i]) > 1e-5)
        {
            fprintf(stderr, "Result verification failed at element %d!\n", i);
            correct = false;
            break;
        }
    }

    fprintf(stdout, "Test %s\n", correct ? "PASSED" : "FAILED");

    // Free device global memory
    CUDA_ERROR_CHECK(BACKUP_cudaFree((void **)&d_A));
    CUDA_ERROR_CHECK(BACKUP_cudaFree((void **)&d_B));
    CUDA_ERROR_CHECK(BACKUP_cudaFree((void **)&d_C));

    // Free host memory
    free(h_A);
    free(h_B);
    free(h_C);

    CUDA_ERROR_CHECK(cudaDeviceReset());

    fprintf(stdout, "Done\n");
    return EXIT_SUCCESS;
}

