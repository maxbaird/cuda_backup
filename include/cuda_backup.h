/**
 * @file cuda_backup.h
 * @brief Function prototypes and macros for the library.
 *
 * This file contains the prototypes and macros needed to interrupt a kernel.
 * Definitions of a few types are also provided.
 *
 * @author Max M. Baird (mmb1@hw.ac.uk)
 * @bug No known bugs.
 */

#ifndef _CUDA_BACKUP_H_
#define _CUDA_BACKUP_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <cuda_runtime.h>

#ifdef HASH_PRINT
  #include "print_hash.h"
#endif /* HASH_PRINT */

/**
 * @brief              Intended to be used as size for char array when printing
 *
 * Intended to be used for specifying the size of a character array to be printed
 * via BACKUP_print to the standard output.
 */
#define BACKUP_BUFFER 256

/**
 * @brief               Indicates the message should be printed at an information level.
 */
#define BACKUP_INFO 1

/**
 * @brief               Indicates the message should be printed at a warning level.
 */
#define BACKUP_WARN 2

/**
 * @brief               Indicates the message should be printed as an error.
 */
#define BACKUP_EROR 3

/**
 * @brief               Indicates the message should be printed only in debug mode.
 */
#define BACKUP_DBUG 4

/**
 * @brief              Typically returned by a function to indicate it was successful.
 */
#define BACKUP_SUCCESS 0

/**
 * @brief              Typically returned by a function if failure occurred.
 */
#define BACKUP_FAILURE (!(BACKUP_SUCCESS))

/**
 * @param[in]          expr   The parameter to be marked as unused.
 * @brief              Used for unused variables to suppress warnings.
 */
#define UNUSED(expr) do { (void)(expr); } while (0)

/**
 * @brief              Used as a type to either do data transfers from kernel or not.
 *
 * This is a simple enum with two members that act as boolean types for #BACKUP_config() to
 * indicate whether memory copies should be performed at each interrupt or not.
 */

typedef enum
{
  backupDoMemcpyTrue,  /**< Do device to host transfers at every interrupt. */
  backupDoMemcpyFalse  /**< Don't do any device to host transfers at interrupt */
} backupDoMemcpy;

/**
 * @brief              Used as a type to increase quantum or not.
 *
 * Enum with two members that act as boolean types that can be passed to #BACKUP_config()
 * to indicate if the quantum should be increased or not.
 */
typedef enum
{
  backupIncreaseQuantumTrue,    /**< Increase quantum after each interrupt */
  backupIncreaseQuantumFalse    /**< Don't increase quantummemory copies  */
} backupIncreaseQuantum;

/**
 * @brief              Used as a type for a callback function to be called at each kernel interrupt.
 */
typedef void(*interrupt_callback)(void *);

/**
 * @brief              Used to re-write kernel launch.
 * @param[in]          kernel_name    The name of the kernel.
 * @param[in]          grid_dim       Specifies the dimension of the kernel grid.
 * @param[in]          block_dim      Specifies the dimension of each block.
 * @param[in]          ns             specifies the number of bytes in shared
 *                                    memory that is dynamically allocated per
 *                                    block for this call in addition to the
 *                                    statically allocated memory.
 * @param[in]          s              Of type cudaStream_t and specifies the associated stream.
 * @param[in]          ...            Other arguments for the kernel.
 *
 * Rewrites kernel launch in a loop and modifies argument list to include
 * #BACKUP_quantum_expired and #BACKUP_block_info. Since kernel launches are
 * asynchronous #BACKUP_monitor() is called immediately after the kernel is
 * launched. #BACKUP_monitor() waits until #BACKUP_quantum expires and copies
 * the boolean array (#h_has_executed) to the host and checks if all
 * blocks were executed. If all blocks were executed it means the kernel is
 * complete and #BACKUP_complete is set to *true* and the loop terminates.
 * Otherwise, the loop continues until all blocks have executed. After the
 * kernel is complete #BACKUP_cleanup() is called to clean up allocated
 * resources.
 *
 * @remark #BACKUP_config() __must__ be called a priori.
 * @remark *ns* and *s* must be specified and can be set to 0 if not used.
 */

#define BACKUP_LAUNCH_MACRO(thread_level, kernel_name, grid_dim, block_dim, ns, s, ...)                     \
  do{                                                                                                       \
    if(BACKUP_config_called)                                                                                \
    {                                                                                                       \
      int ret;                                                                                              \
      ret = BACKUP_try(BACKUP_kernel_init(&BACKUP_quantum_expired, &BACKUP_block_info,                      \
                       &BACKUP_complete, grid_dim, block_dim, thread_level), "initialize");                 \
      if(ret != BACKUP_SUCCESS)                                                                             \
      {                                                                                                     \
        BACKUP_print("Running kernel without interrupts", BACKUP_WARN);                                     \
        kernel_name<<<grid_dim, block_dim, ns, s>>>(NULL, NULL, ## __VA_ARGS__);                            \
      }                                                                                                     \
      else                                                                                                  \
      {                                                                                                     \
        size_t count = 0;                                                                                   \
        char str[BACKUP_BUFFER];                                                                            \
        while(BACKUP_complete == false)                                                                     \
        {                                                                                                   \
          BACKUP_dbug_println();                                                                            \
          sprintf(str, "%s interrupts = %zu", #kernel_name, count);                                         \
          BACKUP_print(str, BACKUP_DBUG);                                                                   \
          kernel_name<<<grid_dim, block_dim, ns, s>>>(BACKUP_quantum_expired,                               \
              BACKUP_block_info, ## __VA_ARGS__);                                                           \
          ret = BACKUP_monitor(&BACKUP_complete);                                                           \
          if(ret != BACKUP_SUCCESS)                                                                         \
          {                                                                                                 \
            BACKUP_print("Monitoring of kernel execution failed", BACKUP_EROR);                             \
          }                                                                                                 \
          count = count + 1;                                                                                \
        }                                                                                                   \
        BACKUP_cleanup(#kernel_name);                                                                       \
      }                                                                                                     \
    }                                                                                                       \
    else                                                                                                    \
    {                                                                                                       \
      BACKUP_print("BACKUP_config() was not called. Doing default kernel launch", BACKUP_WARN);             \
      kernel_name<<<grid_dim, block_dim, ns, s>>>(NULL, NULL, ## __VA_ARGS__);                              \
    }                                                                                                       \
  }while(0)

/**
 * @brief             A wrapper for #BACKUP_LAUNCH_MACRO for block-level interrupts
 *
 * This macro is safe to call for all kernels as interrupts are only permitted at
 * the block level. However, if the kernel does not require synchronisation within
 * blocks via __syncthreads() then #BACKUP_KERNEL_LAUNCH_THREAD can be used in tandem
 * with #BACKUP_CONTINUE_THREAD
 */
#define BACKUP_KERNEL_LAUNCH(kernel_name, grid_dim, block_dim, ns, s, ...)                                  \
  BACKUP_LAUNCH_MACRO(false, kernel_name, grid_dim, block_dim, ns, s, ## __VA_ARGS__)

/**
 * @brief             A wrapper for #BACKUP_LAUNCH_MACRO for thread-level interrupts.
 *
 * @remark This MACRO must be used in conjunction with #BACKUP_CONTINUE_THREAD
 */
#define BACKUP_KERNEL_LAUNCH_THREAD(kernel_name, grid_dim, block_dim, ns, s, ...)                           \
  BACKUP_LAUNCH_MACRO(true, kernel_name, grid_dim, block_dim, ns, s, ## __VA_ARGS__)
/**
 * @brief              Rewrites the kernel's definition.
 * @param[in]          kernel_name    The name of the kernel.
 * @param[in]          ...            The rest of the kernel's arguments.
 *
 * Modifies the kernel's definition to augment the argument list with
 * #BACKUP_quantum_expired and #BACKUP_block_info.
 */
#define BACKUP_KERNEL_DEF(kernel_name, ...)                                                               \
  kernel_name(volatile bool *qtm_expired, bool *has_executed, ## __VA_ARGS__)

/**
 * @brief              Determines whether block should execute or not.
 *
 * This macro should be placed in the first line of the kernel's body because
 * it introduces the code that checks the quantum to determine if the block
 * should execute or not. It also checks the boolean array to determine if the
 * block has previously executed.
 */
#define BACKUP_PROCEED()                                                                                  \
do{                                                                                                       \
  /* These can be NULL if BACKUP_config is not called prior to kernel launch */                           \
  if(qtm_expired != NULL && has_executed != NULL)                                                         \
  {                                                                                                       \
    __shared__ bool quantum_expired;                                                                      \
    unsigned long long int bid = blockIdx.x + gridDim.x * (blockIdx.y + gridDim.z * blockIdx.z);          \
    if (threadIdx.x == 0 && threadIdx.y == 0 && threadIdx.z == 0)                                         \
    {                                                                                                     \
       quantum_expired = *qtm_expired;                                                                    \
    }                                                                                                     \
                                                                                                          \
    if(has_executed[bid])                                                                                 \
    {                                                                                                     \
      return;                                                                                             \
    }                                                                                                     \
    __syncthreads();                                                                                      \
                                                                                                          \
    if(quantum_expired && has_executed[bid] == false)                                                     \
    {                                                                                                     \
      return;                                                                                             \
    }                                                                                                     \
    has_executed[bid] = true;                                                                             \
  }                                                                                                       \
}while(0)


/**
 * @brief              Determines whether thread should execute or not.
 *
 * This macro should be placed in the first line of the kernel's body because
 * it introduces the code that checks the quantum to determine if the thread 
 * should execute or not. It also checks the boolean array to determine if the
 * thread has previously executed.
 */
#define BACKUP_PROCEED_THREAD()                                                                           \
  do{                                                                                                     \
    /* These can be NULL if BACKUP_config is not called prior to kernel launch */                         \
    if(qtm_expired != NULL && has_executed != NULL)                                                       \
    {                                                                                                     \
      int blockId = blockIdx.x + blockIdx.y * gridDim.x                                                   \
      + gridDim.x * gridDim.y * blockIdx.z;                                                               \
      int threadId = blockId * (blockDim.x * blockDim.y * blockDim.z)                                     \
      + (threadIdx.z * (blockDim.x * blockDim.y))                                                         \
      + (threadIdx.y * blockDim.x) + threadIdx.x;                                                         \
                                                                                                          \
      if(has_executed[threadId]){                                                                         \
        return;                                                                                           \
      }                                                                                                   \
      if(*qtm_expired && has_executed[threadId] == false){                                                \
        return;                                                                                           \
      }                                                                                                   \
      has_executed[threadId] = true;                                                                      \
    }                                                                                                     \
  }while(0)

/**
 * @brief             A wrapper for #BACKUP_PROCEED
 *
 * This uses the default implementation of #BACKUP_PROCEED to perform interrupts
 * at a block level. This implementation is the most generic and will work for all
 * kernels. However, if the kernel does not perform block-level synchronisations
 * via __syncthreads(). Then #BACKUP_CONTINUE_THREADS can be used in tandem with
 * #BACKUP_KERNEL_LAUNCH_THREAD.
 */
#define BACKUP_CONTINUE()                                                                                 \
  BACKUP_PROCEED()

/**
 * @brief            A wrapper for #BACKUP_PROCEED_THREAD
 *
 * This macro calls #BACKUP_PROCEED_THREAD which performs interrupts at a thread level
 * and should only be used for kernels which do not use __syncthreads().
 */
#define BACKUP_CONTINUE_THREAD()                                                                          \
  BACKUP_PROCEED_THREAD()


/**
 * @brief              Enables access to the #BACKUP_PRINT_HASH macro.
 */
#ifdef HASH_PRINT

/**
 * @brief              Used to apply the digest function to data copied from the device.
 * @param[in]          data   The data from which hash will be generated.
 * @param[in]          size   The size of the data in bytes.
 *
 * This MACRO is only exposed if #HASH_PRINT is enabled via CMAKE with `-DENABLE_HASH=ON`.
 */
#define BACKUP_PRINT_HASH(data, size)                                                                     \
do                                                                                                        \
{                                                                                                         \
  BACKUP_try(BACKUP_print_digest((unsigned char *)data, size), "print hash.");                            \
}while(0)
#endif /* HASH_PRINT */

/**
 * @brief              Boolean array to keep track of executed blocks.
 *
 * Created in #BACKUP_kernel_init() with a size matching the number of blocks to be
 * launched. A value of *true* is written at *BACKUP_block_info[ID]* for each
 * executed block where *ID* represents the block's ID. At each interrupt this
 * array is copied back to the host and checked for kernel completion. That is,
 * every position in the array having a value of *true*.
 */
bool *BACKUP_block_info; /* Initialized and then passed at kernel launch */

/**
 * @brief              Used to tell kernel when to cease allowing new blocks to execute.
 *
 * Allocated with cudaHostAlloc() so communication with the kernel does not
 * require any explicit memory copies. Set to *true* after the quantum
 * expires and is reset to *false* if kernel needs to be launched again.
 */
volatile bool *BACKUP_quantum_expired;

/**
 * @brief              Used to indicate if the kernel is complete or not.
 *
 * Initially set to *false*. At each kernel interrupt #BACKUP_block_info is
 * checked to determine if kernel is complete. If kernel is complete
 * *BACKUP_complete* is set to *true* and the loop wrapping kernel execution
 * exits. Otherwise *BACKUP_complete* maintains a value of *false*.
 */
bool BACKUP_complete;

/**
 * @brief              Used to determine if #BACKUP_config() was called.
 *
 * This variable is checked by #BACKUP_KERNEL_LAUNCH(), if false a warning
 * message is printed that #BACKUP_config() was not called and a default,
 * non-modified kernel launch is performed. This means that the kernel will not
 * be interrupted and NULL values are passed for #BACKUP_quantum_expired and
 * #BACKUP_block_info. #BACKUP_CONTINUE() first makes a check to ensure that
 * both of these are not NULL before continuing.
 */
bool BACKUP_config_called;

/* host functions */
int BACKUP_kernel_init(volatile bool **qtm_expired, bool **b_info, bool *complete, dim3 num_blocks, dim3 num_threads, bool thread_level);
int BACKUP_monitor(bool *complete);
void BACKUP_cleanup(const char *kernel_name);
cudaError_t BACKUP_cudaMalloc(void** devPtr, size_t size);
cudaError_t BACKUP_cudaFree(void **devPtr);
int BACKUP_config(double q, backupDoMemcpy m, backupIncreaseQuantum increase_q, double q_inc);
void BACKUP_write_to_fs(bool enable);

void BACKUP_register_callback(interrupt_callback callback, void *args);
void BACKUP_stop_kernel_relaunch();
void BACKUP_stop_interrupting_kernel();

void safeFree (void **ptr);
void BACKUP_print(const char *msg, unsigned short int priority);
int BACKUP_try(int result, const char *msg);
void BACKUP_dbug_println();

#ifdef __cplusplus
}
#endif

#endif /* _CUDA_BACKUP_H_ */
