/** 
 * @file allocation_list.c
 * @brief Function definitions for the allocation list.
 *
 * These function definitions implement the prototypes
 * found in allocation_list.h. 
 *
 * @author Max M. Baird (mmb1@hw.ac.uk)
 * @bug No known bugs.
 */

#include <cuda_runtime.h>
#include <stdbool.h>
#include "allocation_list.h"
#include "list/list.h"
#include "cuda_backup.h"
#include "write.h"

/**
 * @brief              Determines if a CUDA API call resulted in error.
 *
 * A customised error checking macro is needed here that does not return
 * BACKUP_FAILURE on error. This is because the functions that need to use this
 * macro have a void return type since they are callback functions applied to
 * each element when traversing the allocation list.
 */
#define CHECK_CUDA_ERRORS(fun)                                                                  \
do{                                                                                             \
    cudaError_t err = fun;                                                                      \
    if(err != cudaSuccess)                                                                      \
    {                                                                                           \
      char str[BACKUP_BUFFER];                                                                  \
      sprintf(str, "Cuda error %d %s:: %s\n", __LINE__, __func__, cudaGetErrorString(err));     \
      BACKUP_print(str, BACKUP_EROR);                                                           \
    }                                                                                           \
}while(0);

/**
 * @brief              The list used to store allocations.
 */
static List l = NULL;

/**
 * @brief              Used to determine if data should be written to file system.
 *
 * If enabled, data transferred at kernel interruption will be written
 * to the file system.
 */
static bool fs_write = false;

/**
 *  @brief             Comparator callback function to compare two list elements.
 *  @param[in]         e1   The first element to compare.
 *  @param[in]         e2   The second element to compare.
 *  @return            int  The result of the comparsion. 
 *
 *  This function is a callback function used by the underlying list
 *  implementation to compare two elements of the list. Zero is returned if the
 *  elements are equal, otherwise a non-zero value is returned.
 */
static int comparator(const void *e1, const void *e2)
{
  allocation_t *m1 = (allocation_t *)e1;
  allocation_t *m2 = (allocation_t *)e2;

  return (m1->dev_ptr == m2->dev_ptr) ? 0 : 1;
}

/**
 * @brief              Frees any data allocated for list elements.
 * @param[in]          element    A reference to the allocation element to be freed.
 *
 * This function frees the dynamically allocated buffer that needed to be
 * allocated when creating a list element. It is a callback used when the
 * entire list needs to be deleted and called for each element of the list.
 */
static void free_allocation_buffer(void *element)
{
  allocation_t *m = (allocation_t *)element;

  /* Only writes if this option was enabled */
  if(fs_write == true)
  {
    BACKUP_try(delete_backup(m->buffer), "delete backup data file");
  }
  BACKUP_print("Freeing backup buffer", BACKUP_DBUG);
  safeFree((void **)&m->buffer);
}

/** 
 * @brief              Frees the entire list of allocations. 
 *
 * This function is registered to be called when the program exits. It
 * cleans up any remaining elements in the allocation list. Ideally, if
 * allocations made by BACKUP_cudaMalloc() are freed with BACKUP_cudaFree()
 * the list will be empty and this function has no resources to clean up.
 */
static void free_allocation_list()
{
  LST_deleteList(l, free_allocation_buffer);
  l = NULL;
}

/** 
 * @brief              Prints a representation of a allocation_t structure.
 * @param[in]          element    A reference to the allocation_t element
 * @param[in]          args       Currently unused
 */
static void print_allocation_t(void *element, void *args)
{
  UNUSED(args);
  char str[BACKUP_BUFFER];
  allocation_t *m = (allocation_t *)element;
  sprintf(str, "dev_ref:[%p] size:%zu", m->dev_ptr, m->size);
  BACKUP_print(str, BACKUP_DBUG);
}

/** 
 * @brief              Performs intermediate device-to-host transfers.
 * @param[in]          element    A reference to the allocation_t element.
 * @param[in]          args       Currently unused.
 *
 * This function is called for each element in the allocation list to perform
 * the device to host memory transfer. If writing to the file system is
 * enabled, the copied data is then written to disk.
 */
static void device_to_host(void *element, void *args)
{
  UNUSED(args);
  char str[BACKUP_BUFFER];

  allocation_t *m = (allocation_t *)element;

  sprintf(str, "Backing up from: %p", m->dev_ptr);
  BACKUP_print(str, BACKUP_DBUG);

  CHECK_CUDA_ERRORS(cudaMemcpy(m->buffer, m->dev_ptr, m->size, cudaMemcpyDeviceToHost));

  if(fs_write == true)
  {
    BACKUP_try(write_backup(m->buffer, m->size), "write backup data");
  }
}

/**
 * @brief             Prints each element in allocation list. 
 *
 * This function iterates over the allocation list and calls a print function
 * for each element within the list. The print function prints to standard out
 * the values of each element.
 */
void ALLOCATION_LST_print_list()
{
  LST_traverse(l, print_allocation_t, NULL);
}

/**
 * @brief              Inserts an entry into the allocation list.
 * @param[in]          dev_ref      A reference to memory allocated on the device.
 * @param[in]          size         The size of the allocated memory.
 */
void ALLOCATION_LST_add(void **dev_ref, size_t size)
{
  /* Check if list has previously been created */
  char str[BACKUP_BUFFER];
  if(l == NULL)
  {
    /* If list has not been previously created
     * then create and register clean up function
     * to be called upon application exit
     */
    l = LST_makeEmptyList(l, comparator);
    atexit(free_allocation_list);
  }

  allocation_t *m = (allocation_t *)malloc(sizeof(allocation_t));

  if(m == NULL)
  {
    sprintf(str, "Failed to allocate memory for allocation_t entry");
    BACKUP_print(str, BACKUP_EROR);
  }

  /* Store the address of device memory. This is
   * necessary so that we can iterate over the
   * list to perform DTH memory transfers when
   * backing up device data. 
   */
  m->dev_ptr = *dev_ref; 
  m->size = size;

  /* Allocate host memory to be used for intermediate transfers */
  m->buffer = (void *)malloc(size);

  if(m->buffer == NULL)
  {
    sprintf(str, "Failed to allocate m->buffer for intermediate transfers");
    BACKUP_print(str, BACKUP_EROR);
  }

  sprintf(str, "Adding allocation node %p", m->dev_ptr);
  BACKUP_print(str, BACKUP_DBUG);
  LST_insert((void *)m, l, LST_first(l));
}

/**
 * @brief              Traverses the allocation list to make an intermediate transfer.
 * @param[in]          write_to_fs    Specifies whether the data should be written to the file system.
 *
 * This function iterates over the allocation list and performs a
 * device-to-host copy of all data allocated so far.
 */
void ALLOCATION_LST_backup_device(bool write_to_fs)
{
  char str[BACKUP_BUFFER];
  sprintf(str, "Write to fs: %s", (write_to_fs) ? "True" : "False");
  BACKUP_print(str, BACKUP_DBUG);

  /* If the allocation list is empty this means that the wrapper functions
   * were not called but copying from memory has been enabled. This will
   * cause a segmentation fault because an empty list will tried to be
   * traversed.
   */
  if(l == NULL)
  {
    BACKUP_print("Allocation list empty, were wrapper functions used? Not backing up device data.", BACKUP_DBUG);
  }
  else
  {
    fs_write = write_to_fs;
    LST_traverse(l, device_to_host, NULL);
  }
}

/**
 * @brief              Removes an allocation element from the list.
 * @param[in]          ref    A reference to the memory allocated on the device.
 *
 * This function is called in BACKUP_cudaFree() and removes the allocation from
 * the list of allocations.
 */
int ALLOCATION_LST_remove(void **ref)
{
  if(l == NULL)
  {
    BACKUP_print("Allocation list empty, were wrapper functions used?", BACKUP_DBUG);
    return BACKUP_FAILURE;
  }

  char str[BACKUP_BUFFER];
  allocation_t temp = {*ref, 0, NULL};
  Position p = LST_find((void *)&temp, l);

  if(p == NULL)
  {
    sprintf(str, "Allocation node %p not found for removal", temp.dev_ptr);
    BACKUP_print(str, BACKUP_WARN);
    return BACKUP_FAILURE;
  }

  /* Get allocation_t structure by pointer to the host address */
  allocation_t *m = (allocation_t *)LST_retrieve(p);

  sprintf(str, "Removing node allocation node %p", m->dev_ptr);
  BACKUP_print(str, BACKUP_DBUG);

  /* Delete associated file on disk (if any) and allocated buffer for backup data*/
  free_allocation_buffer((void *)m);
  LST_deleteNode((void *)m, l, NULL);
  return BACKUP_SUCCESS;
}
