/** 
 * @file allocation_list.h
 * @brief Function prototypes for the allocation list.
 *
 * This contains the prototypes for the allocation list. These functions serve
 * as an interface to manage the allocation list. There is no explicit creation
 * and destruction of this list. The list is created automatically on the first
 * all to ALLOCATION_LST_add() and is cleaned up automatically with every call
 * to BACKUP_cudaFree and/or when the program exits. At every interrupt of the
 * kernel the allocations recorded in this list are used to initiate a device
 * to host transfer of the GPU's data and write this data to file if the option
 * is enabled.
 *
 * @author Max M. Baird (mmb1@hw.ac.uk)
 * @bug No known bugs.
 */

#ifndef _ALLOCATION_LIST_H_
#define _ALLOCATION_LIST_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/** 
 * @brief              This structure is used to keep track of arguments to cudaMalloc.
 *
 * This is important when it comes to saving device data for
 * each suspension and restoring this data upon resumption.
 * The memory member is used to preserve intermediate results
 * from transfers which occur when the kernel is suspended. This
 * is host allocated memory which matches the size of the memory
 * created on the device.
 */
typedef struct
{
  void* dev_ptr;    /**< Address of data on device. */
  size_t size;      /**< The size of the data at the device address. */
  void *buffer;     /**< Buffer to which intermediate transfers will be made. */
}allocation_t;

void ALLOCATION_LST_add(void **dev_ref, size_t size);
void ALLOCATION_LST_print_list();
void ALLOCATION_LST_backup_device(bool write_to_fs);
int ALLOCATION_LST_remove(void **ref);

#ifdef __cplusplus
}
#endif

#endif /* _ALLOCATION_LIST_H */
