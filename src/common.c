/** 
 * @file common.c
 * @brief Function definitions for prototypes in cuda_backup.h.
 *
 * This file holds the function definitions of internal
 * global functions for the library.
 *
 *
 * @author Max M. Baird (mmb1@hw.ac.uk)
 * @bug No known bugs.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cuda_backup.h"

/** Define RED colour for output.                                    */
#define RED   "\x1B[31m"
/** Define YELLOW colour for output.                                 */
#define YEL   "\x1B[33m"
/** Define CYAN colour for output.                                   */
#define CYN   "\x1B[36m"
/** Define WHITE colour for output.                                  */
#define WHT   "\x1B[37m"
/** Define RESET colour for output.                                  */
#define RESET "\x1B[0m"

/**
 * @brief              A wrapper for free().
 * @param[in]          ptr  The pointer address to be freed.
 *
 * Safely frees allocated memory. If ptr is not NULL and the 
 * address that ptr points to is also not NULL, then the memory
 * is freed and the pointer is set to NULL. This means any 
 * consecutive calls will be ignored.
 */
void safeFree (void **ptr)
{
  if (ptr != NULL && *ptr != NULL)
  {
    free(*ptr);
    *ptr = NULL;
  }
}

/**
 * @brief              Prints library messages.
 * @param[in]          msg        The text to print.
 * @param[in]          priority   The priority of msg. 
 *
 * This function is used to do all printing of messages. Messages are
 * printed based on their priority with their respective colours. Messages
 * with DBUG priority are only printed if the library was compiled with the
 * debug option enabled. 
 */
void BACKUP_print(const char *msg, unsigned short int priority)
{
  if(msg != NULL)
  {
    fflush(stdout); /* In case stdout and stderr are the same */
    fflush(stderr);
    switch(priority)
    {
      case BACKUP_INFO: 
        fprintf(stdout, "[ " CYN "BACKUP Info" RESET " ] : %s \n", msg);
        break;
      case BACKUP_WARN: 
        fprintf(stdout, "[ " YEL "BACKUP Warning" RESET " ] : %s \n", msg);
        break;
      case BACKUP_EROR:
        fprintf(stderr, "[ " RED "BACKUP Error" RESET " ] : %s \n", msg);
        break;
      case BACKUP_DBUG:
#ifdef DEBUG
        fprintf(stdout, "[ " WHT "BACKUP Debug" RESET " ] : %s \n", msg);
#endif
        break;
    }

    fflush(stdout);
    fflush(stderr);
  }
}

/**
 * @brief              Prints a newline only if called in debug mode.
 *
 * Necessary if debug printing needs to occur within macros since the macros
 * will not work with nested ifdef checks for DEBUG mode.
 */
void BACKUP_dbug_println()
{
#ifdef DEBUG
  fprintf(stdout, "\n");
  fflush(stdout);
#endif
}

/**
 * @brief              Prints success of failure based on result.
 * @param[in,out]      result   The result to check.
 * @param[in]          msg      The message to be printed.
 * @return             int      Returns the value of result.
 *
 * This function checks the return value of the function and prints a
 * message indicating whether the function succeeded or failed.
 * Successful actions are printed in DBUG mode while failures are printed
 * as warnings.
 */
int BACKUP_try(int result, const char *msg)
{
  char str[BACKUP_BUFFER];

  if(result == BACKUP_SUCCESS)
  {
    sprintf(str, "BACKUP succeeded to %s", msg);
    BACKUP_print(str, BACKUP_DBUG);
  }
  else
  {
    sprintf(str, "BACKUP failed to %s", msg);
    BACKUP_print(str, BACKUP_WARN);
  }

  return result;
}
