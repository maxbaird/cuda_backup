/** 
 * @file cuda_backup.c
 * @brief Interface functions for the library.
 *
 * Here the main interface functions for the library are defined.
 *
 * @author Max M. Baird (mmb1@hw.ac.uk)
 * @bug No known bugs.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdbool.h>
#include <cuda_runtime.h>
#include "cuda_backup.h"
#include "cuda_error.h"
#include "allocation_list.h"

/** 
 * @brief              Used to determine the size of boolean array.
 *
 * The boolean array is used for book-keeping to keep track of executed
 * blocks.
 */
static size_t block_amt  = 0;

/**
 * @brief              Determines how frequently the kernel is interrupted.
 */
static useconds_t quantum = 0.0;

/**
 * @brief              Keeps track of the value initially passed to the quantum.
 *
 * During configuration (see #BACKUP_config()), if the quantum is set to increase
 * but no increase value is specified, the quantum is increased per interrupt by
 * its initial amount. I.e the amount assigned to #initial_quantum. The quantum
 * needs to be increased if it is so small that it expires before the kernel can
 * start blocks not previously executed.
 */
static useconds_t initial_quantum = 0.0; 

/**
 * @brief              Represents the size in bytes of the boolean array.
 *
 * Initialized in #BACKUP_kernel_init and is used later in BACKUP_monitor when
 * performing a device to host transfer of the boolean array (#h_has_executed). 
 */
static size_t block_info_bytes = 0;

/**
 * @brief              Holds a reference to the timeout variable.
 *
 * Assigned to *true* in #BACKUP_monitor() when the quantum has expired so that
 * the kernel can know it should allow new blocks to proceed.  
 */
static volatile bool *quantum_expired = NULL;

/**
 * @brief              Host-side boolean array.
 *
 * A boolean array with size #block_amt. Each value in the array represents a
 * thread block and is set to *true* when the block has finished executing. Is
 * copied to host at each interrupt and checked to determine if all blocks have
 * executed.
 */
bool *h_has_executed = NULL;

/**
 * @brief              Device-side boolean array. See #h_has_executed.
 */
bool *d_has_executed = NULL;

/**
 * @brief              Used to indicate whether the quantum should be increased or not. 
 *
 * It is sometimes necessary to have the quantum increase after each interrupt
 * otherwise the kernel may never finish. If the initial quantum is very small, 
 * the kernel will execute only a few blocks before a timeout occurs. Upon relaunch,
 * the kernel will still schedule the already executed blocks but these blocks will
 * terminate upon checking #d_has_executed. Now if the quantum is not increased,
 * the kernel may spend all of the quantum going through previously executed blocks
 * and have no time to launch new ones. This can continue infinitely; increasing the
 * quantum prevents this.
 */
static backupIncreaseQuantum increase_quantum = backupIncreaseQuantumFalse;

/**
 * @brief               Used to indicate whether memory copies should be done at each interrupt.
 */
static backupDoMemcpy do_memcpy = backupDoMemcpyFalse;

/**
 * @brief              Used to specify the amount quantum should be increased by.
 *
 * If not set to zero and #increase_quantum is set to true, the quantum is increased
 * by this amount at each interrupt. If set to zero and #increase_quantum is true, then
 * the quantum is increased by #initial_quantum at each kernel interrupt.
 */
static double quantum_inc = 0.0;

/**
 * @brief              Determines whether data should be written to file system or not.
 *
 * If set to true, data copied from the device at each interrupt is written to the file
 * system.
 */
static bool write_to_fs = false;

/**
 * @brief              Keeps track of how many times the kernel was interrupted.
 *
 * This value is printed when the kernel has completed.
 */
static size_t suspension_count;

/**
 * @brief              This struct holds a reference to the call back function and its args.
 *
 * Enapsulates the interrupt callback function and its arguments. This allows for the
 * an easy function callback at each kernel interrupt.
 */
typedef struct{
  interrupt_callback callback;  /**< Pointer to callback function */
  void *args;                   /**< Arguments needed by callback function */
}callback_fun_t;

/**
 * @brief              Holds a reference to the struct with the interrupt callback function details. 
 */
static callback_fun_t callback_fun = {NULL, NULL};

/**
 * @brief              Determines whether an interrupt callback function was registered or not.
 *
 * If set to *true*, the callback function specified is called.
 */
static bool callback_registered = false;

/**
 * @brief              Is set in #BACKUP_stop_kernel_relaunch().
 */
static bool stop_kernel_relaunch = false;

/**
 * @brief             Used to stop future kernel interrupts.
 */
static bool stop_interrupting_kernel = false;

/** 
 * @brief              Determines if kernel is complete by checking #h_has_executed. 
 * @param[in,out]      complete   Indicates whether kernel is complete or not.
 *
 * Sets *complete* to *true* or *false* respectively if the kernel is finished or not.
 */
static inline void computation_complete(bool *complete)
{
  size_t i = 0;
  for(i = 0; i < block_amt; i++)
  {
    if(h_has_executed[i] == false){break;}
  }
  *complete = (i == block_amt);
}

/**
 * @brief              Converts seconds to microseconds.
 * @param[in]          quantum  Represents the time to convert in seconds
 * @return             The time in microseconds
 *
 * This function is used to convert the quantum, which is specified in seconds,
 * to microseconds. Microseconds are needed because usleep() is used to be able to
 * give the kernel shorter timeouts than 1 second (as would be the case if sleep() was used).
 */
static inline useconds_t seconds_to_microseconds(double quantum)
{
  return fabs(quantum) * 1000000.0; 
}

/** 
 * @brief              Frees host and device allocated memory
 * @return             #BACKUP_SUCCESS or #BACKUP_FAILURE on success or failure respectively.
 *
 * Frees any memory that was allocated to help execute and interrupt the kernel.
 */
static inline int free_memory()
{
  safeFree((void **)&h_has_executed);

  /* MACROS surrounding CUDA functions return BACKUP_FAILURE on error. */
  CHECK_CUDA_ERRORS(cudaFree(d_has_executed));
  CHECK_CUDA_ERRORS(cudaFreeHost((void *)quantum_expired));

  return BACKUP_SUCCESS;
} 

/**
 * @brief              Resets all global variables to their default value.
 * @return             #BACKUP_SUCCESS or #BACKUP_FAILURE on success or failure respectively.
 */
static inline int reset_globals()
{
  /* Reset all globals */
  block_amt = 0;
  quantum = 0.0;
  initial_quantum = 0.0;
  block_info_bytes = 0;
  quantum_expired = NULL;
  h_has_executed = NULL;
  d_has_executed = NULL;
  suspension_count = 0;
  increase_quantum = backupIncreaseQuantumFalse;
  do_memcpy = backupDoMemcpyFalse;
  quantum_inc = 0.0;

  /* Globals from cuda_backup.h */
  BACKUP_config_called = false;

  /* Values associated with registered interrupt callback function */
  callback_fun.callback = NULL;
  callback_fun.args = NULL;
  callback_registered = false;

  stop_kernel_relaunch = false;
  stop_interrupting_kernel = false;

  return BACKUP_SUCCESS;
}

/**
 * @brief              Initializes the backup library.
 * @param[in,out]      qtm_expired  Pinned variable used by host to communicate with kernel.
 * @param[in,out]      b_info       The boolean array to be passed to kernel. 
 * @param[in,out]      complete     Track reference to tell kernel execution loop when to quit.
 * @param[in]          num_blocks   The number of blocks used in the kernel launch.
 * @return             #BACKUP_SUCCESS or #BACKUP_FAILURE for success or failure respectively. 
 *
 * This function sets up the library to interrupt the kernel. Variables to track the kernel
 * execution are initialized and kept track of. Of particular importance is the *timeout* and 
 * boolean array (*b_info*). *qtm_expired* is allocated using cudaHostAlloc() so that the host
 * may communicate with the device directly without an explicit memory copy. The boolean array
 * has a size of *num_blocks* and has a record of which blocks have executed at each interrupt.
 */
int BACKUP_kernel_init(volatile bool **qtm_expired, bool **b_info, bool *complete, dim3 num_blocks, dim3 num_threads, bool thread_level)
{
  char str[BACKUP_BUFFER];
  BACKUP_print("Initialized backup", BACKUP_DBUG);

  /* Set default values */
  suspension_count = 0;
  size_t i = 0;
  *complete = false;

  block_amt = num_blocks.x * num_blocks.y * num_blocks.z; 

  if(thread_level){
    block_amt  = block_amt * num_threads.x * num_threads.y * num_threads.z;
  }

  /* Block info host setup */
  block_info_bytes = block_amt * sizeof(bool);
  h_has_executed = (bool *)malloc(block_info_bytes);

  if(h_has_executed == NULL)
  {
    sprintf(str, "Cannot allocate %zu bytes for block info!", block_info_bytes);
    BACKUP_print(str, BACKUP_EROR);
    return BACKUP_FAILURE;
  }

  for(i = 0; i < block_amt; i++)
  {
    h_has_executed[i] = false;
  }

  CHECK_CUDA_ERRORS(cudaHostAlloc((void **)&(*qtm_expired), sizeof(volatile bool), cudaHostAllocMapped));

  /* Block info device setup */
  CHECK_CUDA_ERRORS(cudaMalloc((void **)&(*b_info), block_info_bytes));
  CHECK_CUDA_ERRORS(cudaMemcpy(*b_info, h_has_executed, block_info_bytes, cudaMemcpyHostToDevice));

  **qtm_expired = false;

  /* Keep track of some things locally */
  quantum_expired = *qtm_expired;
  d_has_executed = *b_info;
  
  return BACKUP_SUCCESS;
}

/**
 * @brief              Prints how many times the kernel was suspended. 
 * @param[in]          kernel_name  The name of the kernel.
 */
static inline void print_stats(const char *kernel_name)
{
  char str[BACKUP_BUFFER];
  sprintf(str,"%s suspensions = %zu", kernel_name, suspension_count);
  BACKUP_print(str, BACKUP_INFO);
}

/**
 * @brief              Prints kernel suspension count and frees memory.
 * @param[in]          kernel_name    The name of the kernel.
 * @return             #BACKUP_SUCCESS or #BACKUP_FAILURE if successful or not.
 *  
 * This function calls #print_stats() and #free_memory().
 */
static inline int cleanup(const char *kernel_name)
{
  int res;
  print_stats(kernel_name);
  res = BACKUP_try(free_memory(), "free memory");

  /* Reset global variables to their defaults */
  BACKUP_try(reset_globals(), "reset global variables");

  return res;
}

/**
 * @brief              Calls #cleanup().
 * @param[in]          kernel_name    The name of the kernel.
 *
 * An interface function to clean up allocated resources and print
 * how often *kernel_name* was suspended.
 */
void BACKUP_cleanup(const char *kernel_name)
{
  int ret = BACKUP_try(cleanup(kernel_name), "cleaning up resources");
  
  if(ret != BACKUP_SUCCESS)
  {
    BACKUP_print("Failed to properly clean up resources", BACKUP_EROR);
  }
}

/**
 * @brief           Waits for quantum to expire.
 *
 * Used in #BACKUP_monitor when initially waiting for the kernel to expire. If
 * the specified quantum is in minutes this function loops to perform a
 * cudaEventQuery every second; this is to handle the case of the kernel
 * returning before the quantum has expired so that no long period of time is
 * spent idly waiting.
 */
static inline void wait()
{
  char str[BACKUP_BUFFER];
  int q = quantum / 1000000.0f; /* Convert to seconds */

  /* If quantum is in seconds or minutes check kernel every second */
  if(q != 0)
  {
    cudaEvent_t event;
    cudaEventCreate(&event);
    cudaEventRecord(event, 0);
    cudaError_t err;
    while(q)
    {
      err = cudaEventQuery(event); 
      sprintf(str, "Event Query: %s", cudaGetErrorString(err));
      BACKUP_print(str, BACKUP_DBUG);
      if(err == cudaSuccess)
      {
        break;
      }
      sleep(1);
      q--;
    }
    cudaEventDestroy(event);
  }
  else
  {
    usleep(quantum);
  }
}

/**
 * @brief             Signals the GPU to return and then waits for it to return.
 * @param[in,out]     complete      Initialized to *true* or *false* if all blocks
 *                                  in kernel have finished or not
 * @return             #BACKUP_SUCCESS or #BACKUP_FAILURE for success or failure respectively.
 *
 * Called in #BACKUP_monitor() so tell the GPU it should finish the currently
 * executing blocks and prevent new blocks from continuing on to their main
 * body of work.
 */
static inline int signal_gpu_then_wait(bool *complete)
{
  char str[BACKUP_BUFFER];

  BACKUP_print("Signalling kernel to return...", BACKUP_DBUG);
  *quantum_expired = true;

  BACKUP_print("Waiting on kernel...", BACKUP_DBUG);
  CHECK_CUDA_ERRORS(cudaDeviceSynchronize());
  BACKUP_print("Kernel came back...", BACKUP_DBUG);

  CHECK_CUDA_ERRORS(cudaMemcpy(h_has_executed, d_has_executed, block_info_bytes, cudaMemcpyDeviceToHost)); 
  BACKUP_print("Checking if complete", BACKUP_DBUG);
  computation_complete(complete);

  sprintf(str, "Done checking: %d", *complete);
  BACKUP_print(str, BACKUP_DBUG);

  return BACKUP_SUCCESS;
}

/**
 * @brief            Executed when control is returned to host after GPU is interrupted or finished.
 * @parm[in]         complete          Used to check if kernel finished executing all blocks or not. 
 *
 * This function handles the control returned to the host after the GPU has finished executing. It
 * also executes any callback function to be executed at GPU interrupt. The callback function is
 * only executed if the GPU is not finished. This function also increases the quantum by a default
 * or specified amount. This increase is necessary in cases where the quantum is short enough to 
 * cause the GPU to return again by the time it has finished re-launching previously executed blocks.
 */
static inline void handle_gpu_suspension(bool *complete)
{
  char str[BACKUP_BUFFER];
  if(*complete == false)
  {

    if(callback_registered)
    {
      BACKUP_print("Calling callback function", BACKUP_DBUG);
      callback_fun.callback(callback_fun.args);
    }

    BACKUP_print("Incomplete, resuming", BACKUP_DBUG);
    *quantum_expired = false;

    if(increase_quantum == backupIncreaseQuantumTrue)
    {
      if(fabs(quantum_inc) < 1e-8)
      {
        quantum = quantum + initial_quantum;
      }
      else
      {
        quantum = quantum + seconds_to_microseconds(quantum_inc);
      }
    }

    if(do_memcpy == backupDoMemcpyTrue)
    {
      BACKUP_print("Backing up device", BACKUP_DBUG);
      ALLOCATION_LST_backup_device(write_to_fs);
    }

    sprintf(str, "Quantum is now: %u", quantum);
    BACKUP_print(str, BACKUP_DBUG);
    suspension_count++; 

    if(stop_kernel_relaunch)
    {
      BACKUP_print("Stopping kernel relaunch as requested", BACKUP_DBUG);
      *complete = true;
    }
  }
}

/**
 * @brief              Monitors the kernel's execution until it completes.
 * @param[in,out]      complete           Initialized to *true* or *false* if the kernel has finished or not. 
 * @return             #BACKUP_SUCCESS or #BACKUP_FAILURE for success or failure respectively.
 *
 * Sleeps until the quantum has expired and then sets the quantum_expired
 * variable to *true* and waits for the kernel to return. The value in
 * quantum_expired is immediately available to the kernel which then does not
 * allow any new blocks to execute.  After the kernel returns, the boolean
 * array is copied to the host and checked to determine if all blocks executed.
 * If all blocks are finished *complete* is set to *true* and no further kernel
 * launches are made.  Otherwise this process repeats iteratively.
 *
 * If the quantum is in seconds or larger the kernel's execution is checked
 * every second for completion. This is avoids the case of the host sleeping
 * unnecessarily when a large quantum is set and the kernel completes very
 * early.
 */
int BACKUP_monitor(bool *complete)
{
  int ret;

  if(stop_interrupting_kernel)
  {
    /* Wait on kernel if it should no longer be interrupted */
    BACKUP_print("Kernel will no longer be interrupted, waiting..", BACKUP_DBUG);
    CHECK_CUDA_ERRORS(cudaDeviceSynchronize());
    *complete = true;
    return BACKUP_SUCCESS;
  }

  /* Wait for quantum to expire */
  wait();
  
  /* Tell GPU to finish and come back now */
  ret = BACKUP_try(signal_gpu_then_wait(complete), "signal and wait on kernel");

  if(ret != BACKUP_SUCCESS)
  {
    BACKUP_print("Failed to signal and wait on kernel", BACKUP_EROR);
    return BACKUP_FAILURE;
  }

  /* Handle interrupted GPU */
  handle_gpu_suspension(complete);

  return BACKUP_SUCCESS;
}

/**
 * @brief              Library configurations that must be done before kernel launch.
 * @param[in]          q            Quantum, time to wait before interrupting kernel.
 * @param[in]          m            Whether to perform device to host transfers at each interrupt.
 * @param[in]          increase_q   Whether to increase the quantum or not.
 * @param[in]          q_inc        The amount to increase the quantum by.
 * @return             #BACKUP_SUCCESS or #BACKUP_FAILURE for success or failure respectively.
 *
 * This function needs to be called before the launch of every kernel using the interrupt
 * mechanism to set execution parameters.
 */
int BACKUP_config(double q, backupDoMemcpy m, backupIncreaseQuantum increase_q, double q_inc)
{
  if(BACKUP_config_called)
  {
    BACKUP_print("Updating backup configuration", BACKUP_DBUG);
  }

  if(fabs(q) < 1e-8)
  {
    BACKUP_print("Quantum <= zero; defaulting to 60 seconds", BACKUP_WARN);
    q = 60.0;
  }

  quantum = seconds_to_microseconds(q);
  initial_quantum = quantum;

  do_memcpy = m;
  increase_quantum = increase_q;
  quantum_inc = q_inc;

  BACKUP_config_called = true;
  
  return BACKUP_SUCCESS;
}

/**
 * @brief              Toggle writing of kernel data to file system.
 * @param[in]          enable   True or false to either write or not.
 *
 * Whether or not to write data transferred from the kernel at interrupt to
 * the file system or not.
 */
void BACKUP_write_to_fs(bool enable)
{
  write_to_fs = enable;

  if(write_to_fs)
  {
    BACKUP_print("Writing backup data to file system enabled. This is experimental!", BACKUP_WARN);
  }
}

/**
 * @brief               Registeres callback function to be called at each kernel interrupt.
 * @param[in]           callback  A reference to the callback function to be called.
 * @param[in]           args      Arguments needed by the callback function if necessary.
 * 
 * Initializes the callback struct with callback reference and args. Also sets the value
 * of #callback_registered to *true* to indicate that the callback should be called at each
 * kernel interrupt.
 */
void BACKUP_register_callback(interrupt_callback callback, void *args)
{
   BACKUP_print("Interrupt callback registered", BACKUP_DBUG);
   callback_fun.callback = callback;
   callback_fun.args = args;
   callback_registered = true;
}

/**
 * @brief               When called, stops kernel from re-launching after an interrupt.
 *
 * This is useful in cases where failing fast is important, all data is copied to host
 * as usual but kernel is not relaunched.
 */
void BACKUP_stop_kernel_relaunch()
{
  stop_kernel_relaunch = true;
}

/**
 * @brief               When called, future interrupts of the kernel are disabled.
 */
void BACKUP_stop_interrupting_kernel()
{
  stop_interrupting_kernel = true;
}
