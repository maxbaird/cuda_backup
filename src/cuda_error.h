/** 
 * @file cuda_error.h
 * @brief Contains macros/functions to be used for CUDA error checking.
 *
 * @author Max M. Baird (mmb1@hw.ac.uk)
 * @bug No known bugs.
 */

#ifndef _CUDA_ERROR_H_
#define _CUDA_ERROR_H_

/**
 * @brief              Checks return value for CUDA API functions for error.
 * @param[in]          fun    The function whose return value will be checked.
 *
 * Wraps CUDA API functions to check their return values. Any value other than 
 * cudaSuccess prints a warning with the error message and returns BACKUP_FAILURE.
 */
#define CHECK_CUDA_ERRORS(fun)                                                                  \
do{                                                                                             \
    cudaError_t err = fun;                                                                      \
    if(err != cudaSuccess)                                                                      \
    {                                                                                           \
      char str[BACKUP_BUFFER];                                                                  \
      sprintf(str, "Cuda error %d %s:: %s\n", __LINE__, __func__, cudaGetErrorString(err));     \
      BACKUP_print(str, BACKUP_WARN);                                                           \
      return BACKUP_FAILURE;                                                                    \
    }                                                                                           \
}while(0);

#endif /* _CUDA_ERROR_H_ */
