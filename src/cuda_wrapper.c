/** 
 * @file cuda_wrapper.c
 * @brief Wrappers for standard CUDA functions.
 *
 * Functions here are wrappers for the standard CUDA API functions;
 * so their parameters mirror those of the CUDA function being
 * wrapped. Wrapping CUDA API functions allows the library to keep
 * track of the pointers necessary to manage the allocations made
 * on the device and to perform device to host transfers each time
 * the kernel is interrupted.
 *
 * @author Max M. Baird (mmb1@hw.ac.uk)
 * @bug No known bugs.
 */

#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include "cuda_backup.h"
#include "allocation_list.h"

/**
 * @brief              A wrapper for cudaMalloc.
 * @param[in,out]      devPtr       Pointer to allocated device memory.
 * @param[in]          size         Requested allocation size in bytes.
 * @return             cudaError_t  Indicates whether cudaMalloc was successful.
 *
 * Allocates size bytes of linear memory on the device and returns in *devPtr a 
 * pointer to the allocated memory. After calling cudaMalloc() an entry to the
 * allocation list is made to record the pointer to the device memory along with
 * its size.
 */
cudaError_t BACKUP_cudaMalloc(void** devPtr, size_t size)
{
  char str[BACKUP_BUFFER];
  cudaError_t err = cudaMalloc((void **)&(*devPtr), size);

  if(err == cudaSuccess)
  {
    ALLOCATION_LST_add(devPtr, size);
  }
  else
  {
    sprintf(str, "Allocation list not updated. cudaMalloc failed: %s", cudaGetErrorString(err));
    BACKUP_print(str, BACKUP_WARN);  
  }
  return err; 
}

/**
 * @brief              A wrapper for cudaFree.
 * @param[in]          devPtr        Device pointer to memory to free.
 * @return             cudaError_t   Indicates whether cudaFree was successful.
 *
 * Frees the memory space pointed to by devPtr, which must have been returned
 * by a previous call to cudaMalloc(). Otherwise, or if cudaFree(devPtr) has
 * already been called before, an error is returned. If devPtr is 0, no
 * operation is performed. cudaFree() returns cudaErrorInvalidDevicePointer in
 * case of failure.
 */
cudaError_t BACKUP_cudaFree(void** devPtr)
{
  char str[BACKUP_BUFFER];
  sprintf(str, "delete %p from allocation list", *devPtr);
  BACKUP_try(ALLOCATION_LST_remove(devPtr), str);
  return cudaFree(*devPtr);
}
