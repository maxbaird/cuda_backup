/** 
 * @file print_hash.c
 * @brief Implementation to print data hashes.
 *
 * @author Max M. Baird (mmb1@hw.ac.uk)
 * @bug No known bugs.
 */
#include <openssl/evp.h>
#include "cuda_backup.h"

/** 
 * @brief            If enabled, prints errors that may occur from using the OpenSSL library.
 * @param[in]        msg    The error message to be printed.
 */
#define BACKUP_HANDLE_SSL_ERRORS(msg)                 \
do                                                    \
{                                                     \
    char str[BACKUP_BUFFER];                          \
    sprintf(str, "%s", msg);                          \
    BACKUP_print(str, BACKUP_WARN);                   \
}while(0)

/**
 * @brief              Digests the referenced data and prints the hash.
 * @param[in]          ptr      A reference to the data to be digested.
 * @param[in]          size     The size of the data to be digested.
 * @return             BACKUP_SUCCESS or BACKUP_FAILURE respectively on success or failure.
 *
 * If the library is built with debugging hash printing enabled, this function
 * is enabled to print the hash of *ptr*. This function is meant to be used to
 * verify that use of the backup library produces the correct result. This
 * means that hashes of the kernel that use the library should be identical to
 * hashes of the kernel that don't, unless the result of the kernel is
 * variable.
 */
int BACKUP_print_digest(const unsigned char *ptr, size_t size)
{
  unsigned int i;
  EVP_MD_CTX *mdctx;
  unsigned int digest_len = EVP_MD_size(EVP_sha512());
  unsigned char *digest;
  char *hash = (char *)malloc(digest_len + digest_len + 1);
  char str[BACKUP_BUFFER];

  if((mdctx = EVP_MD_CTX_create()) == NULL)
  {
    BACKUP_HANDLE_SSL_ERRORS("Error creating MD context");
  }

  if(1 != EVP_DigestInit_ex(mdctx, EVP_sha512(), NULL))
  {
    BACKUP_HANDLE_SSL_ERRORS("Error initializing MD context");
  }

  if(1 != EVP_DigestUpdate(mdctx, ptr, size))
  {
    BACKUP_HANDLE_SSL_ERRORS("Error updating MD context");
  }
  
  if((digest = (unsigned char *)OPENSSL_malloc(digest_len)) == NULL)
  {
    BACKUP_HANDLE_SSL_ERRORS("Error digest memory");
  }

  if(1 != EVP_DigestFinal_ex(mdctx, digest, &digest_len))
  {
    BACKUP_HANDLE_SSL_ERRORS("Error finalizing digest");
  }
  
  for(i = 0; i < digest_len; i++)
  {
    sprintf(&hash[i*2], "%02x", (unsigned int)digest[i]);
  }

  sprintf(str, "Hash of data @ address %p = %s", ptr, hash);
  BACKUP_print(str, BACKUP_INFO);

  safeFree((void **)&hash);
  OPENSSL_free(digest);
  EVP_MD_CTX_destroy(mdctx);

  return BACKUP_SUCCESS;
}
