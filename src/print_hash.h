/** 
 * @file print_hash.h
 * @brief Function prototypes to print data hashes.
 *
 * @author Max M. Baird (mmb1@hw.ac.uk)
 * @bug No known bugs.
 */

#ifndef _PRINT_HASH_
#define _PRINT_HASH_

#ifdef __cplusplus
extern "C" {
#endif

int BACKUP_print_digest(const unsigned char *ptr, size_t size);

#ifdef __cplusplus
}
#endif

#endif /* _PRINT_HASH_ */
