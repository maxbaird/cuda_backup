/** 
 * @file write.c
 * @brief Function definitions for data write functions.
 *
 * These function definitions implement the prototypes
 * found in write.h. 
 *
 * @author Max M. Baird (mmb1@hw.ac.uk)
 * @bug No known bugs.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "write.h"
#include "cuda_backup.h"

/**
 * @brief              Used to specifiy the size of the char array for the file name.
 */
#define BUFF_SIZE 32

/**
 * @brief             Formats the name of the file to write.
 * @param[out]        s   The formatted file name.
 * @param[in]         p   The pointer to the data.
 *
 * This generates a unique file name for the data to be written
 * by appending the memory address of the data to ".bin".
 */
static inline void get_backup_name(char *s, void *p)
{
  sprintf(s,"%p.bin", p); 
}

/**
 * @brief             Writes data to file system.
 * @param[in]         data    Data to write.
 * @param[in]         size    The size of the data to write.
 * @return            integer Whether the write was successful or not.
 *
 * This function writes intermediate data copied from the device
 * at every interrupt to the file system.
 */
int write_backup(void *data, size_t size)
{
  char path[BUFF_SIZE];
  char str[BACKUP_BUFFER];
  get_backup_name(path, data);

  FILE *fptr = fopen(path, "wb");
  size_t written = 0;
  
  if(fptr == NULL)
  {
    sprintf(str, "Failed to open %s for writing!", path);
    BACKUP_print(str, BACKUP_EROR);
    return BACKUP_FAILURE;
  }

  written = fwrite(data, sizeof(unsigned char), size, fptr);

  if(written != size)
  {
    sprintf(str, "Failed to write file %s of size %zu : %s", path, size, strerror(errno));
    BACKUP_print(str, BACKUP_WARN);
    fclose(fptr);
    return BACKUP_FAILURE;
  }

  fclose(fptr);
  return BACKUP_SUCCESS;
}

/**
 * @brief              Deletes data from file system. 
 * @param[in]          data The address of the data to delete.
 * @return             integer Whether the delete was successful or not.
 *
 * This function reconstructs the file name from its memory address and
 * removes it from the file system.
 */
int delete_backup(void *data)
{
  char path[BUFF_SIZE];
  char str[BACKUP_BUFFER];
  get_backup_name(path, data);

  sprintf(str, "Removing backup file: %s", path);
  BACKUP_print(str, BACKUP_DBUG);

  if(remove(path) == -1)
  {
    sprintf(str, "Failed deleting backup file: %s : %s", path, strerror(errno));
    BACKUP_print(str, BACKUP_WARN);
    return BACKUP_FAILURE;
  }

  return BACKUP_SUCCESS;
}
