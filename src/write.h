/** 
 * @file write.h
 * @brief Prototypes for the functions which write and delete data.
 * 
 * These functions are responsible for the writing and deleting of
 * data transferred intermittently from the GPU. The data is only
 * written if explicitly enabled through the function BACKUP_write_to_fs().
 *
 * @author Max M. Baird (mmb1@hw.ac.uk)
 * @bug No known bugs.
 */

#ifndef _WRITE_H_
#define _WRITE_H_

#ifdef __cplusplus
extern "C" {
#endif

int write_backup(void *data, size_t size);
int delete_backup(void *data);

#ifdef __cplusplus
}
#endif

#endif /* _WRITE_H_ */
